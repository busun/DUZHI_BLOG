package me.duzhi.blog.controller;

import com.jfinal.aop.Before;
import com.jfinal.kit.PathKit;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import io.jpress.core.JBaseController;
import io.jpress.core.addon.Addon;
import io.jpress.core.addon.AddonClassLoader;
import io.jpress.core.addon.AddonInfo;
import io.jpress.core.addon.AddonManager;
import io.jpress.core.interceptor.ActionCacheClearInterceptor;
import io.jpress.interceptor.UCodeInterceptor;
import io.jpress.router.RouterMapping;
import io.jpress.router.RouterNotAllowConvert;
import me.duzhi.blog.addon.DAddOn;
import me.duzhi.blog.addon.TemplateConfigure;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author ashang.peng@aliyun.com
 * @date 二月 18, 2017
 */

@RouterMapping(url = "/admin/caddon", viewPath = "/WEB-INF/admin/caddon")
@Before(ActionCacheClearInterceptor.class)
@RouterNotAllowConvert
public class _AddonConfigController  extends JBaseController {

    public void index() {
        List<DAddOn> addOns = new ArrayList<DAddOn>();
        List<AddonInfo> addonList = AddonManager.me().getAddons();
        for (AddonInfo addonInfo : addonList) {
            Addon addon = addonInfo.getAddon();
            for (AddonInfo info : addonList) {
                DAddOn addOn = new DAddOn();
                addOn.setAddon(info);
                String html = getConfigTemplate(addonInfo);
                if (html == null) {
                    continue;
                }
                addOn.setHtml(html);
                TemplateConfigure.me().putTemplate(info.getId(), html);
                StringWriter writer = new StringWriter();
                try {
                    Template template = TemplateConfigure.me().getTemplate(info.getId(), "utf-8");
                    template.process(new HashMap(), writer);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TemplateException e) {
                    e.printStackTrace();
                }
                addOn.setHtml(writer.toString());
                addOns.add(addOn);
            }
        }
        setAttr("addons", addOns);
    }
    @Before(UCodeInterceptor.class)
    public void settingsave() {
        keepPara();
        renderAjaxResultForSuccess("成功！");
    }

    private String getConfigTemplate(AddonInfo addon) {
        try {
            String webRoot = PathKit.getWebRootPath();
            StringBuilder newFileName = new StringBuilder(webRoot).append(addon.getJarPath());
            AddonClassLoader classLoader = new AddonClassLoader(newFileName.toString());
            File file = null;
            JarFile jar = new JarFile(webRoot + addon.getJarPath());
            String packageDIR = addon.getAddon().getClass().getPackage().getName();
            packageDIR = packageDIR.replace(".", "/");
            String resourceName = packageDIR + "/config.html";
            JarEntry jarEntry = jar.getJarEntry(resourceName);
            InputStream inputStream = jar.getInputStream(jarEntry);
            InputStreamReader isr = new InputStreamReader(inputStream, "UTF-8");
            if (inputStream != null) {
                String str = "";
                StringBuffer sb = new StringBuffer();
                BufferedReader reader = new BufferedReader(isr);

                while ((str = reader.readLine()) != null) {
                    sb.append(str);
                }

                return sb.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
