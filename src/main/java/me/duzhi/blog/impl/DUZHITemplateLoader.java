package me.duzhi.blog.impl;

import freemarker.cache.WebappTemplateLoader;

import javax.servlet.ServletContext;

/**
 * @author ashang.peng@aliyun.com
 * @date 二月 19, 2017
 */

public class DUZHITemplateLoader extends WebappTemplateLoader{

    public DUZHITemplateLoader(ServletContext servletContext) {
        super(servletContext);
    }

    public DUZHITemplateLoader(ServletContext servletContext, String subdirPath) {
        super(servletContext, subdirPath);
    }


}
