package me.duzhi.blog;

import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import me.duzhi.blog.hanlder.HoldHandler;
import me.duzhi.blog.hanlder.HtmlHandler;


/**
 * @author ashang.peng@aliyun.com
 * @date 一月 07, 2017
 */

public class Config extends io.jpress.Config {
    @Override
    public void onJPressStarted() {
        super.onJPressStarted();
    }

    public void configHandler(Handlers handlers) {
        handlers.add(new HoldHandler());
        handlers.add(new HtmlHandler());
        super.configHandler(handlers);
    }

    @Override
    public void configInterceptor(Interceptors interceptors) {
        interceptors.add(new me.duzhi.blog.interceptor.MemberInterceptor());
        super.configInterceptor(interceptors);
    }
}
